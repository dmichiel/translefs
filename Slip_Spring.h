#ifndef SLIP_SPRING_H
#define SLIP_SPRING_H

#include <random>
#include <time.h>

class Slip_Spring {

	public:

	 Slip_Spring();
	
	 void load(int an,int ho, char dir);
	 void unload();

	 void move(int h_new);

	 int get_anchor();
	 int get_hook();
	 char get_dir();

	 void change_dir(char d_new);

	 bool is_active();

	 ~Slip_Spring();


	private:

	 int a;
	 int h;

	 char direction;	//u for extruding towards the largest id, d for extruding towards 1 

	 bool active;
};

#endif
