#include "SSLmp_functions.h"
#include "Slip_Spring.h"
#include "rnd_gen.h"

#include "mpi.h"

#include "lammps.h"         // these are LAMMPS include files
#include "domain.h"
#include "group.h"
#include "input.h"
#include "atom.h"
#include "library.h"


#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>


using namespace std;


vector<Slip_Spring*> SSprings;

int RW1,RW2;
int Nb; //Number of beads
int Ns = 1; //1 condensin (slip-spring)
double lr, ur, sp;  //loading/unloading rates (0,1) (probabilities of successfull loading/unloading)
		    //and speed (0,1) (probability of successfull SS update)

bool conf = false;
double R;  //Confinement radius

bool fixed_ends = false;

int Tts = 1.5e+7;   //total time steps
int Uts	= 4.0e+03;  //time steps between slip-springs update
int Dts	= 2.0e+04;  //time steps between dumps

stringstream line, iname, dname, tname;

ofstream ofile;

int main(int argc, char **argv) {

	cout << "START." << endl;
	cout << "*****************************************" << endl;
	cout << "NOTE1: initial configurations should be already equilibrated" << endl;
	cout << "NOTE2: new confined or fixed end options" << endl;
	cout << "they should be added after the main arguments" << endl;
	cout << "-c R : polymer confined in a sphere, R is the radius" << endl;  //adds indent fix
	cout << "-f : polymer with fixed end" << endl;  //removes the extremities of the polymer from the nve integrator
	cout << "*****************************************" << endl;

	//check main arguments

	if(argc < 7) {

		cout << "*****************************************" << endl;
		cout << "Usage: " << endl;
		cout << "./EXE_NAME RW1 RW2 Nb lr ur sp -c R -f" << endl;
		cout << "RW1 RW2: repetition number interval" << endl;
		cout << "Nb: number of beads" << endl;
		cout << "lr: loading probability" << endl;
		cout << "ur: unloading probability" << endl;
		cout << "sp: speed" << endl;
		cout << "*****************************************" << endl;

		return 1;

	}

	RW1 = atoi(argv[1]);
	RW2 = atoi(argv[2]);
	Nb = atoi(argv[3]);
	lr = atof(argv[4]);
	ur = atof(argv[5]);
	sp = atof(argv[6]);

	int opt = 0;
	while((opt=getopt(argc,argv,"c:f")) != -1) {
		switch(opt) {
			case 'c':
			 conf = true;
			 R = atof(optarg);
			 break;

			case 'f':
			 fixed_ends = true;
			 break;

			default : 
			 break;
		}
	}


	// setup MPI istance

  	int me,nprocs;
  	MPI_Init(&argc,&argv);
  	MPI_Comm_rank(MPI_COMM_WORLD,&me);
  	MPI_Comm_size(MPI_COMM_WORLD,&nprocs);


	//Create Slip_Springs

	for(int i = 0; i < Ns; i++) {

		Slip_Spring* slsp = new Slip_Spring;
		SSprings.push_back(slsp);
	}

	for(int rep = RW1; rep <= RW2; rep++) {

		// setup LAMMPS istance
		stringstream line;
		line << "SS.screen.RW" << rep << ".log";	
		LAMMPS_NS::LAMMPS *lmp;
 		char * arguments[2];
		arguments[0] = NULL;
		arguments[1] = "-screen";

		//note: c_str() doesn't work here because it returns a const char. We need a workaround:
	
		string not_going_out_of_scope;
		not_going_out_of_scope = line.str();
		arguments[2] = &(not_going_out_of_scope[0]);		
		line.str("");
		line.clear();
		lmp = new LAMMPS_NS::LAMMPS(3,arguments,MPI_COMM_WORLD);
	
		cout << "RW: " << rep << " Lammps Initialised" << endl;

		// setup seeds, variable RW and the name of the output directory
		line << "variable seed1 equal " << seed_dist(rnd_gen);
		lmp->input->one(line.str().c_str());
		line.str("");
		line.clear();	
		line << "variable RW equal " << rep;
		lmp->input->one(line.str().c_str());		

		//lmp->input->one("variable dirname index ../TRAJECTORIES"); //where trajectories are saved
		//lmp->input->one("shell mkdir ${dirname}"); 
		//lmp->input->one("shell mkdir ${dirname}/EQUI"); //where equilibrium trajectories are saved
		cout << "Variables set" << endl;
		
		//set sim parameters

		lmp->input->one("units lj");
		lmp->input->one("atom_style angle");
		lmp->input->one("boundary        p p p");
		lmp->input->one("neighbor 1.6 bin");

		lmp->input->one("pair_style  lj/cut 2.5");
		lmp->input->one("bond_style hybrid fene harmonic");
		lmp->input->one("angle_style cosine");

		lmp->input->one("neigh_modify every 1 delay 1 check yes");

		cout << "Settings and styles set" << endl;
		
		//read initial conf
		
		stringstream r_name;

		r_name << "Equi/In_conf.Nb" << Nb;		
		if(conf) r_name << ".R" << R;
		r_name << ".RW" << rep;
		if(fixed_ends) r_name << ".fixed";
		r_name << ".dat"; //read file name

		cout << r_name.str() << endl;

		line.str("");
		line.clear();	
		line << "read_data " << r_name.str() << " extra/bond/per/atom 2";
		lmp->input->one( line.str().c_str() );

		r_name.str("");
		r_name.clear();
		cout << "Data read" << endl;

		if(fixed_ends) {	//fix extremities if fixed

			line.str("");
			line.clear();		
			line << "group temp1 type 1";
			//for(int i = 0; i < Nb; i++) line << " " << i+1;
			lmp->input->one( line.str().c_str() );	
			line.str("");
			line.clear();		
			line << "group temp2 id 1 " << Nb; //fix both ends
			lmp->input->one( line.str().c_str() );	

			lmp->input->one( "group dna subtract temp1 temp2" );	

			lmp->input->one( "group temp1 delete" );
			lmp->input->one( "group temp2 delete" );
		}

		else {
			lmp->input->one( "group dna type 1" );
		}


		if(conf) {	//fix indent if confined

			line.str("");
			line.clear();
			line << "variable R equal " << R;
			lmp->input->one(line.str().c_str());
			lmp->input->one("fix spin all indent 1.0 sphere 0.0 0.0 0.0 ${R} side in");
		}


		line.str("");
		line.clear();	
		line << "Param.lam";
		lmp->input->file( line.str().c_str() );		

		cout << "equilibration Rep " << rep << "done" << endl;

		//setup dump

		dname << "Slip_Springs.Nb" << Nb << ".Ns" << Ns;		
		if(conf) dname << ".R" << R;
		dname << ".RW" << rep;
		if(fixed_ends) dname << ".fixed";
		//dname << ".lammpstrj"; //dump file name
		dname << ".xyz"; //dump file name

		line.str("");
		line.clear();
	
		//line << "dump 1 all custom " << Dts << " " << dname.str() << " id mol type x y z ix iy iz";
		line << "dump 1 all xyz " << Dts << " " << dname.str();
		lmp->input->one( line.str().c_str() );
		
		dname.str("");
		dname.clear();
		cout << "Dump set" << endl;	

		tname << "Slip_Springs.JumpStory.Nb" << Nb << ".Ns" << Ns;		
		if(conf) tname << ".R" << R;
		tname << ".RW" << rep;
		if(fixed_ends) tname << ".fixed";
		//dname << ".lammpstrj"; //dump file name
		tname << ".dat"; //dump file name	

		ofile.open(tname.str().c_str());

		int nc = Tts/Uts;

		int time = 0;

		lmp->input->one( "run 100000" );
		lmp->input->one( "timestep 0.0001" );	

		//main cycle

		for(int i = 0; i < nc; i++) {	
			//new seed for each iteration
			lmp->input->one( "unfix 2" );
			line.str("");
			line.clear();
	
			lmp->input->one( "variable seed2 delete" );
			line << "variable seed2 equal " << seed_dist(rnd_gen);
			lmp->input->one( line.str().c_str() );	
			lmp->input->one( "fix 2 dna  langevin   1.0 1.0 1.0  ${seed2}" );	

			line.str("");
			line.clear();

			//update slip-springs
			move_slip_springs(Nb, lmp, SSprings, sp, lr, ur, time, ofile);
		
			line << "run " << Uts;
			lmp->input->one( line.str().c_str() );

			time+=Uts;
			if((i%10) == 0) cout << "progress: " << (i*1.0)/nc*100 << "%" << endl;	

		}

		line.str("");
		line.clear();
		ofile.close();

		//unload slip-springs for new repetition

		for(int i = 0; i < SSprings.size(); i++) {

			if(SSprings[i]->is_active()) SSprings[i]->unload();
		}


		delete lmp;

	}

	cout << "Done!" << endl;
	MPI_Finalize();
	return 0;
}
