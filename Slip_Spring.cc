#include "Slip_Spring.h"
#include "rnd_gen.h"


using namespace std;

Slip_Spring::Slip_Spring() {

	a = -1;
	h = -1;

	active = false;

}


Slip_Spring::~Slip_Spring() {

}

void Slip_Spring::load(int an,int ho, char dir) {

	a = an;
	h = ho;

	direction = dir;

	active = true;

}

void Slip_Spring::unload() {

	a= -1;
	h = -1;

	active = false;

}

void Slip_Spring::move(int h_new) {

	h = h_new;
}

void Slip_Spring::change_dir(char d_new) {

	if(d_new != 'u' && d_new != 'd') direction = 'u';
	direction = d_new;
}


int Slip_Spring::get_anchor() {return a;}

int Slip_Spring::get_hook() {return h;}

char Slip_Spring::get_dir() {return direction;}



bool Slip_Spring::is_active() {return active;}
