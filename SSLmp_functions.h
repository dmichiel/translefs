#ifndef SSLMPFUNCTIONS_H
#define SSLMPFUNCTIONS_H

#include "mpi.h"

#include "lammps.h"         // these are LAMMPS include files
#include "domain.h"
#include "group.h"
#include "input.h"
#include "atom.h"
#include "library.h"

#include "Slip_Spring.h"

#include <vector>
#include <fstream>

void print_topo(std::vector<Slip_Spring*> &SSprings, int time, std::ofstream& ofile);

void move_slip_springs(int Nb, LAMMPS_NS::LAMMPS *lmp, std::vector<Slip_Spring*> &SSprings, double speed, double loading_rate, double unloading_rate, int time, std::ofstream& ofile);

double dist(double *u, double *v);

int overlap(int b1, int b2, int Nb, std::vector<Slip_Spring*> &SSprings, int sn);

#endif
