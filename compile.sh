echo Loading mpi
module load mpi
echo Compiling
g++-4.8 -I/home/andrea/opt/openmpi/include -pthread -I /home/andrea/Desktop/Condensins_StepSize/lammps_src -o SlIp_SpRiNgAmI Slip_Spring.cc SSLmp_functions.cc Slip_Springami.cc  -pthread -Wl,-rpath -Wl,/home/andrea/opt/openmpi/lib -Wl,--enable-new-dtags -L/home/andrea/opt/openmpi/lib -lmpi /home/andrea/Desktop/Condensins_StepSize/lammps_src/liblammps_mpi.a -std=c++11
echo Done
